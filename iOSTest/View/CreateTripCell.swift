//
//  CreateTripCell.swift
//  iOSTest
//
//  Created by Apple on 6/2/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

protocol SwitchActionDelegate: class {
    func switchValueChanged(value: Bool)
}

protocol VehicleOptionDelegate: class {
    func vehicleOptionSelected()
}

class CreateTripCell: UITableViewCell {

    @IBOutlet weak var txtField: BottomBorderTextField!
    @IBOutlet weak var txtView: BorderedTextView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtPickupDate: BottomBorderTextField!
    @IBOutlet weak var lblSeats: UILabel!
    @IBOutlet weak var paidSwitch: UISwitch!
    @IBOutlet weak var ddTxtField:DesignableUITextField!
    
    weak var switchDelegate: SwitchActionDelegate?
    weak var vehicleDelegate: VehicleOptionDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(cellData: TripCell){
        
        self.lblTitle.font = UIFont.boldSystemFont(ofSize: 16)
        self.lblTitle.text = cellData.title
        
        if cellData.type == "TextField" || cellData.type == "InfoButton" {
            
            self.txtField.keyboardType = .default
            self.txtField.placeholder = cellData.placeHolder
            
            if cellData.type == "InfoButton" {
                self.txtField.setNumberKeypad(target: self, selector: #selector(setAmt))
            }
        }
        if cellData.type == "TextView" {
            self.txtView.delegate = self
            self.txtView.textColor = .lightGray
            self.txtView.text = cellData.placeHolder
        }
        
        if cellData.type == "Switch" {
            
            self.paidSwitch.addTarget(self, action: #selector(paidAction), for: .valueChanged)
        }
        
        if cellData.type == "DatePicker" {
            
            self.txtPickupDate.text = Date().dateString()
            self.ddTxtField.createDatePicker(target: self, selector: #selector(selectedDate))
        }
        
        if cellData.type == "DropDown" {
            
            self.ddTxtField.delegate = self
            self.ddTxtField.tag = 14
        }
    }

    
    
    @objc func paidAction() {
        self.switchDelegate?.switchValueChanged(value: self.paidSwitch.isOn)
    }
    
    @objc func setAmt() {
        self.txtField.resignFirstResponder()
    }
    
    @objc func selectedDate() {
        
        if let datePicker = self.txtField.inputView as? UIDatePicker {
            
            self.ddTxtField.text = datePicker.date.timeString()
        }
        self.ddTxtField.resignFirstResponder()
    }
    
    @IBAction func stepperAction(_ sender: UIStepper) {
        
        print(sender.value)
        
        self.lblSeats.text = String(Int(sender.value))
    }
    
}
extension CreateTripCell: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField.tag == 14 {
            
            vehicleDelegate?.vehicleOptionSelected()
            return false
        }
        return true
    }
}
extension CreateTripCell: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.textColor = UIColor.black
            textView.text = nil
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Select Pickup Location"
            textView.textColor = UIColor.lightGray
        }
    }
}
