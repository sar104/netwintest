//
//  BorderedTextView.swift
//  iOSTest
//
//  Created by Apple on 6/3/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class BorderedTextView: UITextView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.gray.cgColor
    }
}
