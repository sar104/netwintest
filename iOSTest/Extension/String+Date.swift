//
//  String+Date.swift
//  iOSTest
//
//  Created by Apple on 6/2/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

extension String {
    
    func stringToDate() -> Date {
        
        if self.isEmpty {
            return Date()
        }
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        dateFormatterGet.timeZone = TimeZone(identifier: "UTC")
        
        return dateFormatterGet.date(from: self)!
        
    }
    
}

extension Date {
    
    func timeString() -> String {
        
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "hh:mm a"
        return dateformatter.string(from: self)
    }
    
    func dateString() -> String {
        
        let dateformatter = DateFormatter()
        dateformatter.dateStyle = .full
        return dateformatter.string(from: self)
    }
}
