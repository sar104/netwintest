//
//  ViewController.swift
//  iOSTest
//
//  Created by Apple on 6/2/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var mapView: MKMapView!
    
    var arrCellIDs:[TripCell] = []
    var isPaid: Bool = false
    var selVehicle: String = ""
    let pinClrArr = [UIColor.green, UIColor.red]
    var pinLoc:[String] = []
    var pinCnt = 0
    var annotationArr: [MKPointAnnotation] = []
    var arrData:[String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setup()
    }

    func setup() {
        
        mapView.delegate = self
        let longTapGesture = UILongPressGestureRecognizer(target: self, action: #selector(longTap))
        mapView.addGestureRecognizer(longTapGesture)
        
        tableView.rowHeight = 100
        
        var cellData = TripCell(id: "RideNameCell", type: "TextField", placeHolder: "Enter Vehicle Name", title: "Ride Name")
        arrCellIDs.append(cellData)
        
        cellData = TripCell(id: "PickupLocCell", type: "TextView", placeHolder: "Select Pickup Location", title: "Pickup Location")
        arrCellIDs.append(cellData)
        
        cellData = TripCell(id: "PickupDateCell", type: "DatePicker", placeHolder: "Time", title: "Pickup Date Time")
        arrCellIDs.append(cellData)
        
        cellData = TripCell(id: "VehicleTypeCell", type: "DropDown", placeHolder: "", title: "Vehicle Type")
        arrCellIDs.append(cellData)
        
        cellData = TripCell(id: "RideNameCell", type: "TextField", placeHolder: "Enter Vehicle Name", title: "Vehicle Name")
        arrCellIDs.append(cellData)
        
        cellData = TripCell(id: "RideNameCell", type: "TextField", placeHolder: "Enter Vehicle Number", title: "Vehicle Number")
        arrCellIDs.append(cellData)
        
        cellData = TripCell(id: "AvailableSeatsCell", type: "Stepper", placeHolder: "", title: "Available Seats")
        arrCellIDs.append(cellData)
        
        cellData = TripCell(id: "isPaidCell", type: "Switch", placeHolder: "", title: "Is Paid")
        arrCellIDs.append(cellData)
        
        cellData = TripCell(id: "RideNameCell", type: "InfoButton", placeHolder: "Enter Ride Amount", title: "Ride Amount")
        arrCellIDs.append(cellData)
    }

    @objc func longTap(sender: UIGestureRecognizer){
        print("long tap")
        if sender.state == .began {
            
            if pinCnt == 2 {
                pinCnt = 0
                mapView.removeAnnotations(annotationArr)
            }
            
            let locationInView = sender.location(in: mapView)
            let locationOnMap = mapView.convert(locationInView, toCoordinateFrom: mapView)
            addAnnotation(location: locationOnMap)
            
            
        }
    }
    
    func addAnnotation(location: CLLocationCoordinate2D){
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = location
        
        getAddressFromLatLon(pdblLatitude: location.latitude, withLongitude: location.longitude, annotation: annotation)
        
        self.mapView.addAnnotation(annotation)
        annotationArr.append(annotation)
        
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      
        return 9
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let data = arrCellIDs[indexPath.row]
        
        var cell = tableView.dequeueReusableCell(withIdentifier: data.id) as? CreateTripCell
       
        if data.type == "Switch" {
            cell?.switchDelegate = self
            cell?.paidSwitch.isOn = isPaid
        }
        cell?.configureCell(cellData: data)
        
        if data.type == "DropDown" {
            
            cell?.vehicleDelegate = self
            cell?.ddTxtField.text = selVehicle
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 8 && !isPaid {
            
            return 0
        }
        
        if indexPath.row == 1 {
            return 107
        }
        return 80
    }
    
    
    func createOptions() -> UIAlertController {
        
        let typeOptions = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let type1 = UIAlertAction(title: "Two Wheeler", style: .default) { _ in
            self.selVehicle = "Two Wheeler"
            self.tableView.reloadRows(at: [IndexPath(row: 3, section: 0)], with: .none)
        }
        let type2 = UIAlertAction(title: "Four Wheeler", style: .default) { _ in
            self.selVehicle = "Four Wheeler"
            self.tableView.reloadRows(at: [IndexPath(row: 3, section: 0)], with: .none)
        }
        let type3 = UIAlertAction(title: "Other", style: .default) { _ in
            self.selVehicle = "Other"
            self.tableView.reloadRows(at: [IndexPath(row: 3, section: 0)], with: .none)
        }
        let type4 = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            
        }
        
        typeOptions.addAction(type1)
        typeOptions.addAction(type2)
        typeOptions.addAction(type3)
        typeOptions.addAction(type4)
        
        return typeOptions
    }
    
}

extension ViewController: SwitchActionDelegate {
    
    func switchValueChanged(value: Bool) {
        
        isPaid = value
        
        let indexPaths = [IndexPath(row: 7, section: 0), IndexPath(row: 8, section: 0)]
        self.tableView.reloadRows(at: indexPaths, with: .none)
    }
    
}

extension ViewController: VehicleOptionDelegate {
    
    func vehicleOptionSelected() {
        let options = createOptions()
        self.present(options, animated: false, completion: nil)
    }
}

extension ViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        
    }
}

extension ViewController: MKMapViewDelegate{
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard annotation is MKPointAnnotation else { print("no mkpointannotaions"); return nil }
        
        let reuseId = "pin"
        var pinView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId) as? MKPinAnnotationView
        
        if pinView == nil {
            pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            pinView!.canShowCallout = true
            pinView!.rightCalloutAccessoryView = UIButton(type: .infoDark)
            pinView!.pinTintColor = pinClrArr[pinCnt]
        }
        else {
            pinView!.pinTintColor = pinClrArr[pinCnt]
            pinView!.annotation = annotation
            
        }
        print("picnCnt: \(pinCnt)")
        pinCnt = pinCnt + 1
        
        return pinView
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        print("tapped on pin ")
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        if control == view.rightCalloutAccessoryView {
            if let doSomething = view.annotation?.title! {
                print("do something")
            }
        }
    }
    
    func getAddressFromLatLon(pdblLatitude: Double, withLongitude pdblLongitude: Double, annotation: MKPointAnnotation) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = pdblLatitude//Double("\(pdblLatitude)")!
        //21.228124
        let lon: Double = pdblLongitude//Double("\(pdblLongitude)")!
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks! as [CLPlacemark]
                
                if pm.count > 0 {
                    let pm = placemarks![0]
                   
                    var addressString : String = ""
                    if pm.subLocality != nil {
                        //addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.thoroughfare != nil {
                        //addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country!
                    }
                    if pm.postalCode != nil {
                        //addressString = addressString + pm.postalCode! + " "
                    }
                    
                    self.pinLoc.append(addressString)
                    
                    annotation.title = addressString
                    //annotation.subtitle = pm.country!
                    print(self.pinLoc)
                }
        })
        
    }
}
