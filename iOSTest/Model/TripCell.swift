//
//  TripCell.swift
//  iOSTest
//
//  Created by Apple on 6/2/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

struct TripCell {
    
    var id: String
    var type: String
    var placeHolder: String
    var title: String
}
